#!/usr/bin/env python3
from random import randint
import json

fake_data = [
    ['can.beans', -700591459, 2],
    ['cctv.camera', 634478325, 2],
    ['flare', 304481038, 2],
    ['flashlight.held', -196667575, 2],
    ['granolabar', -746030907, 3],
    ['gunpowder', -265876753, 2],
    ['hat.miner', -1539025626, 1],
    ['lowgradefuel', -946369541, 2],
    ['metalblade', 1882709339, 2],
    ['metalpipe', 95950017, 2],
    ['metalspring', -1021495308, 2],
    ['propanetank', -1673693549, 2],
    ['rope', 1414245522, 2],
    ['scrap', -932201673, 3],
    ['sewingkit', 1234880403, 3],
    ['targeting.computer', 1523195708, 2],
    ['techparts', 73681876, 1],
    ['torch', 110547964, 2]
]

number_of_sets = 20
max_item_types = 3

def weights_to_sized_weight(weights_data):
    tot_weight = 0
    cleaned_data_set = []
    for [name, item_id, weight] in weights_data:
        if weight != 0:
            cleaned_data_set.append([name, item_id, weight])

    for [_name, _item_id, weight] in cleaned_data_set:
        tot_weight += weight

    number_of_picks = max_item_types * number_of_sets
    relative_weight = number_of_picks / tot_weight

    # adjust them
    for [name, item_id, weight] in cleaned_data_set:
        yield [name, item_id, int(weight * relative_weight)] # Not sure what rounding to use so..

def generate_loot_tables(data_set):
    sets_dict = {}
    for x in range(number_of_sets):
        randomnumber = randint(1, 100)
        if randomnumber <= 75:
            items_in_set = 1
        elif randomnumber <= 90:
            items_in_set = 2
        else:
            items_in_set = 3
        if items_in_set != 0:
            picked_items = []
            for i in range(items_in_set):
                item_picked = data_set[randint(0, len(data_set)-1)]
                picked_items.append(item_picked[1])
                pos_in_list = data_set.index(item_picked)
                if item_picked[2] -1 == 0:
                    data_set.pop(pos_in_list)
                else:
                    data_set[pos_in_list] = [item_picked[0], item_picked[1], item_picked[2] -1]
            sets_dict[x] = picked_items
        else:
            sets_dict[x] = "0"
    return sets_dict

def write_to_json(loot_table):
    generated_nightminer_dict = {}
    set_names = []
    for x in range(number_of_sets):
        set_names.append("nightminer{}".format(x))
        nightminer_base = {
            "name": "nightminer{}".format(x),
            "description": None,
            "max": 0,
            "cooldown": 0.0,
            "authlevel": 0,
            "hide": False,
            "npconly": True,
            "permission": None,
            "image": None,
            "building": None,
        }
        nightminer_item_base = [
            {
                "itemid": -690276911,
                "container": "wear",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -1539025626,
                "container": "wear",
                "amount": 1,
                "skinid": 917597383,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -702051347,
                "container": "wear",
                "amount": 1,
                "skinid": 899410135,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": 1751045826,
                "container": "wear",
                "amount": 1,
                "skinid": 797128321,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -1549739227,
                "container": "wear",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": 237239288,
                "container": "wear",
                "amount": 1,
                "skinid": 1305364315,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -1302129395,
                "container": "belt",
                "amount": 1,
                "skinid": 1283807232,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -932201673,
                "container": "main",
                "amount": randint(3, 7),
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
        ]
        loot_drop = []
        for item_id in loot_table[x]:
            if item_id != "0":
                amount = 1
                if item_id == -932201673:
                    amount = randint(3, 7)
                elif item_id == -946369541:
                    amount = 10
                elif item_id == -265876753:
                    amount = 3
                json_block = {
                    "itemid": item_id,
                    "container": "main",
                    "amount": amount,
                    "skinid": 0,
                    "weapon": False,
                    "blueprintTarget": 0,
                    "mods": []
                }
                loot_drop.append(json_block)
        nightminer_items = nightminer_item_base + loot_drop
        generated_nightminer_dict["nightminer{}".format(x)] = nightminer_base
        generated_nightminer_dict["nightminer{}".format(x)]["items"] = nightminer_items
    sillykitstring_dict = {"Kit": set_names}
    with open("nightminer_set_names.json", "w") as namefile:
        json.dump(sillykitstring_dict, namefile)
    with open("nightminer_generation.json", "w") as kitfile:
        json.dump(generated_nightminer_dict, kitfile)

data_set = list(weights_to_sized_weight(fake_data))
write_to_json(generate_loot_tables(data_set))