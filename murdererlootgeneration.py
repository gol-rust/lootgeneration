#!/usr/bin/env python3
from random import randint
import json

fake_data = [
    ['ammo.handmade.shell', 588596902, 5],
    ['ammo.nailgun.nails', -2097376851, 5],
    ['ammo.shotgun', -1685290200, 2],
    ['apple', 1548091822, 8],
    ['arrow.bone', 215754713, 6],
    ['arrow.fire', 14241751, 2],
    ['arrow.wooden', -1234735557, 4],
    ['axe.salvaged', -262590403, 1],
    ['bandage', -2072273936, 5],
    ['bearmeat', -1520560807, 5],
    ['bearmeat.cooked', 1873897110, 4],
    ['black.raspberries', 1931713481, 8],
    ['blueberries', -586342290, 8],
    ['bone.armor.suit', 1746956556, 2],
    ['bone.club', 1711033574, 3],
    ['bow.hunting', 1443579727, 3],
    ['bucket.helmet', 850280505, 2],
    ['can.beans', -700591459, 5],
    ['can.tuna', -1941646328, 5],
    ['cctv.camera', 634478325, 1],
    ['chicken.cooked', -1848736516, 4],
    ['chicken.raw', -1440987069, 5],
    ['chocholate', 363467698, 4],
    ['corn', 1367190888, 8],
    ['crossbow', 1965232394, 3],
    ['deer.skull.mask', -1903165497, 2],
    ['deermeat.cooked', -1509851560, 4],
    ['deermeat.raw', 1422530437, 5],
    ['flare', 304481038, 3],
    ['flashlight.held', -196667575, 2],
    ['gears', 479143914, 2],
    ['granolabar', -746030907, 4],
    ['hammer.salvaged', -1506397857, 1],
    ['hat.wolf', -1478212975, 3],
    ['hatchet', -1252059217, 1],
    ['humanmeat.cooked', 1536610005, 6],
    ['icepick.salvaged', -1780802565, 1],
    ['longsword', -1469578201, 2],
    ['mace', -1966748496, 2],
    ['machete', -1137865085, 2],
    ['meat.boar', 621915341, 5],
    ['meat.pork.cooked', -242084766, 4],
    ['metalblade', 1882709339, 2],
    ['metalpipe', 95950017, 2],
    ['metalspring', -1021495308, 2],
    ['mushroom', -1962971928, 8],
    ['pickaxe', -1302129395, 1],
    ['pistol.eoka', -75944661, 3],
    ['pistol.nailgun', 1953903201, 3],
    ['pookie.bear', -1651220691, 1],
    ['propanetank', -1673693549, 3],
    ['pumpkin', -567909622, 7],
    ['roadsigns', 1199391518, 2],
    ['rope', 1414245522, 2],
    ['salvaged.cleaver', -1978999529, 2],
    ['salvaged.sword', 1326180354, 2],
    ['scrap', -932201673, 7],
    ['sewingkit', 1234880403, 2],
    ['sheetmetal', -1994909036, 2],
    ['shotgun.waterpipe', -1367281941, 2],
    ['spear.stone', 1602646136, 2],
    ['syringe.medical', 1079279582, 3],
    ['targeting.computer', 1523195708, 1],
    ['waterjug', -119235651, 5],
    ['weapon.mod.flashlight', 952603248, 1],
    ['weapon.mod.simplesight', -855748505, 1],
    ['wolfmeat.cooked', 813023040, 4],
    ['wolfmeat.raw', -395377963, 5]
]

number_of_sets = 200
max_item_types = 3

def weights_to_sized_weight(weights_data):
    tot_weight = 0
    cleaned_data_set = []
    for [name, item_id, weight] in weights_data:
        if weight != 0:
            cleaned_data_set.append([name, item_id, weight])

    for [_name, _item_id, weight] in cleaned_data_set:
        tot_weight += weight

    number_of_picks = max_item_types * number_of_sets
    relative_weight = number_of_picks / tot_weight

    # adjust them
    for [name, item_id, weight] in cleaned_data_set:
        yield [name, item_id, int(weight * relative_weight)] # Not sure what rounding to use so..

def generate_loot_tables(data_set):
    sets_dict = {}
    for x in range(number_of_sets):
        randomnumber = randint(1, 100)
        if randomnumber <= 35:
            items_in_set = 0
        elif randomnumber <= 75:
            items_in_set = 1
        elif randomnumber <= 90:
            items_in_set = 2
        else:
            items_in_set = 3
        if items_in_set != 0:
            picked_items = []
            for i in range(items_in_set):
                item_picked = data_set[randint(0, len(data_set)-1)]
                picked_items.append(item_picked[1])
                pos_in_list = data_set.index(item_picked)
                if item_picked[2] -1 == 0:
                    data_set.pop(pos_in_list)
                else:
                    data_set[pos_in_list] = [item_picked[0], item_picked[1], item_picked[2] -1]
            sets_dict[x] = picked_items
        else:
            sets_dict[x] = "0"
    return sets_dict

def write_to_json(loot_table):
    generated_murderer_dict = {}
    set_names = []
    for x in range(number_of_sets):
        set_names.append("murderer{}".format(x))
        murderer_base = {
            "name": "murderer{}".format(x),
            "description": None,
            "max": 0,
            "cooldown": 0.0,
            "authlevel": 0,
            "hide": False,
            "npconly": True,
            "permission": None,
            "image": None,
            "building": None,
        }
        murderer_item_base = [
            {
                "itemid": 1877339384,
                "container": "wear",
                "amount": 1,
                "skinid": 1076584212,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -2025184684,
                "container": "wear",
                "amount": 1,
                "skinid": 809704306,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": 1992974553,
                "container": "wear",
                "amount": 1,
                "skinid": 921250017,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -690276911,
                "container": "wear",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": 795236088,
                "container": "belt",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -932201673,
                "container": "main",
                "amount": randint(1, 4),
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
             },
        ]
        loot_drop = []
        for item_id in loot_table[x]:
            if item_id != "0":
                amount = 1
                if item_id == -932201673:
                    amount = randint(1, 3)
                json_block = {
                    "itemid": item_id,
                    "container": "main",
                    "amount": amount,
                    "skinid": 0,
                    "weapon": False,
                    "blueprintTarget": 0,
                    "mods": []
                }
                loot_drop.append(json_block)
        murderer_items = murderer_item_base + loot_drop
        generated_murderer_dict["murderer{}".format(x)] = murderer_base
        generated_murderer_dict["murderer{}".format(x)]["items"] = murderer_items
    sillykitstring_dict = { "Kit": set_names}
    with open("murderer_set_names.json", "w") as namefile:
        json.dump(sillykitstring_dict, namefile)
    with open("murderer_generation.json", "w") as kitfile:
        json.dump(generated_murderer_dict, kitfile)

data_set = list(weights_to_sized_weight(fake_data))
write_to_json(generate_loot_tables(data_set))