# lootgeneration

Datagen is used to generate the data where you insert weights for loot. This information is grabbed from [oxide](https://raw.githubusercontent.com/OxideMod/Oxide.Docs/master/source/includes/rust/item_list.html.md), parsed and output.
