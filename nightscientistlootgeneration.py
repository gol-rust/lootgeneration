#!/usr/bin/env python3
from random import randint
import json

fake_data = [
    ['ammo.pistol.hv', -1691396643, 30],
    ['ammo.rifle.hv', 1712070256, 30],
    ['ammo.shotgun.slug', -727717969, 30],
    ['bandage', -2072273936, 8],
    ['cctv.camera', 634478325, 3],
    ['coffeecan.helmet', -803263829, 2],
    ['explosive.satchel', -1878475007, 1],
    ['explosives', 1755466030, 1],
    ['flamethrower', -1215753368, 1],
    ['flare', 304481038, 6],
    ['flashlight.held', -196667575, 8],
    ['gears', 479143914, 3],
    ['geiger.counter', 999690781, 1],
    ['grenade.f1', 143803535, 3],
    ['hazmatsuit', 1266491000, 2],
    ['hazmatsuit_scientist', -253079493, 1],
    ['keycard_green', 37122747, 2],
    ['largemedkit', 254522515, 6],
    ['longsword', -1469578201, 12],
    ['metalblade', 1882709339, 4],
    ['metalpipe', 95950017, 4],
    ['metalspring', -1021495308, 4],
    ['pistol.m92', -852563019, 1],
    ['pistol.semiauto', 818877484, 1],
    ['riot.helmet', 671063303, 8],
    ['roadsign.jacket', -2002277461, 2],
    ['roadsign.kilt', 1850456855, 2],
    ['roadsigns', 1199391518, 4],
    ['scrap', -932201673, 12],
    ['supply.signal', 1397052267, 1],
    ['syringe.medical', 1079279582, 12],
    ['targeting.computer', 1523195708, 3],
    ['techparts', 73681876, 3],
    ['weapon.mod.holosight', 442289265, 6],
    ['weapon.mod.lasersight', -132516482, 6],
    ['weapon.mod.small.scope', 567235583, 1]
]

number_of_sets = 20
max_item_types = 3

def weights_to_sized_weight(weights_data):
    tot_weight = 0
    cleaned_data_set = []
    for [name, item_id, weight] in weights_data:
        if weight != 0:
            cleaned_data_set.append([name, item_id, weight])

    for [_name, _item_id, weight] in cleaned_data_set:
        tot_weight += weight

    number_of_picks = max_item_types * number_of_sets
    relative_weight = number_of_picks / tot_weight

    # adjust them
    for [name, item_id, weight] in cleaned_data_set:
        yield [name, item_id, int(weight * relative_weight)] # Not sure what rounding to use so..

def generate_loot_tables(data_set):
    sets_dict = {}
    for x in range(number_of_sets):
        randomnumber = randint(1, 100)
        if randomnumber <= 75:
            items_in_set = 1
        elif randomnumber <= 90:
            items_in_set = 2
        else:
            items_in_set = 3
        if items_in_set != 0:
            picked_items = []
            for i in range(items_in_set):
                item_picked = data_set[randint(0, len(data_set)-1)]
                picked_items.append(item_picked[1])
                pos_in_list = data_set.index(item_picked)
                if item_picked[2] -1 == 0:
                    data_set.pop(pos_in_list)
                else:
                    data_set[pos_in_list] = [item_picked[0], item_picked[1], item_picked[2] -1]
            sets_dict[x] = picked_items
        else:
            sets_dict[x] = "0"
    return sets_dict

def write_to_json(loot_table):
    generated_scientist_dict = {}
    set_names = []
    for x in range(number_of_sets):
        set_names.append("nightscientist{}".format(x))
        scientist_base = {
            "name": "nightscientist{}".format(x),
            "description": None,
            "max": 0,
            "cooldown": 0.0,
            "authlevel": 0,
            "hide": False,
            "npconly": True,
            "permission": None,
            "image": None,
            "building": None,
        }
        scientist_item_base = [
            {
                "itemid": -253079493,
                "container": "wear",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -690276911,
                "container": "wear",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": 1588298435,
                "container": "belt",
                "amount": 1,
                "skinid": 947954942,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -1469578201,
                "container": "belt",
                "amount": 1,
                "skinid": 855009078,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -932201673,
                "container": "main",
                "amount": randint(4, 10),
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
        ]
        loot_drop = []
        for item_id in loot_table[x]:
            if item_id != "0":
                amount = 1
                if item_id == -932201673 or item_id == -1691396643 or item_id == 1712070256 or item_id == -727717969:
                    amount = randint(1, 3)
                json_block = {
                    "itemid": item_id,
                    "container": "main",
                    "amount": amount,
                    "skinid": 0,
                    "weapon": False,
                    "blueprintTarget": 0,
                    "mods": []
                }
                loot_drop.append(json_block)
        scientist_items = scientist_item_base + loot_drop
        generated_scientist_dict["nightscientist{}".format(x)] = scientist_base
        generated_scientist_dict["nightscientist{}".format(x)]["items"] = scientist_items
    sillykitstring_dict = { "Kit": set_names}
    with open("nightscientist_set_names.json", "w") as namefile:
        json.dump(sillykitstring_dict, namefile)
    with open("nightscientist_generation.json", "w") as kitfile:
        json.dump(generated_scientist_dict, kitfile)

data_set = list(weights_to_sized_weight(fake_data))
write_to_json(generate_loot_tables(data_set))
