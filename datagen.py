import urllib.request

def data_table():
    url = 'https://raw.githubusercontent.com/OxideMod/Oxide.Docs/master/source/includes/rust/item_list.html.md'
    response = urllib.request.urlopen(url)
    data = response.read()
    text = data.decode('utf-8')
    text = text.replace(" ", "").split('\n')[4:]

    data_list = []
    for i in text:
        o=i.split("|")
        try:
            data_list.append([o[3], int(o[1]), 0])
        except Exception:
            pass
    return data_list

for item in data_table():
    print("{},".format(item))