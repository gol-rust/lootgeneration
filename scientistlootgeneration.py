#!/usr/bin/env python3
from random import randint
import json

fake_data = [
    ['ammo.pistol', 785728077, 30],
    ['ammo.rifle', -1211166256, 30],
    ['ammo.shotgun', -1685290200, 30],
    ['antiradpills', -1432674913, 8],
    ['axe.salvaged', -262590403, 5],
    ['bandage', -2072273936, 8],
    ['burlap.gloves', 1366282552, 8],
    ['cctv.camera', 634478325, 3],
    ['coffeecan.helmet', -803263829, 2],
    ['explosive.satchel', -1878475007, 1],
    ['explosives', -592016202, 1],
    ['flamethrower', -1215753368, 1],
    ['flare', 304481038, 6],
    ['flashlight.held', -196667575, 8],
    ['gears', 479143914, 3],
    ['grenade.beancan', 1840822026, 3],
    ['grenade.f1', 143803535, 3],
    ['hat.miner', -1539025626, 8],
    ['hazmatsuit', 1266491000, 2],
    ['hazmatsuit_scientist', -253079493, 1],
    ['hoodie', 1751045826, 8],
    ['icepick.salvaged', -1780802565, 4],
    ['jacket.snow', -48090175, 8],
    ['keycard_green', 37122747, 2],
    ['largemedkit', 254522515, 6],
    ['metal.refined', 317398316, 4],
    ['metalblade', 1882709339, 4],
    ['metalpipe', 95950017, 4],
    ['metalspring', -1021495308, 4],
    ['pants', 237239288, 8],
    ['pistol.m92', -852563019, 1],
    ['pistol.python', 1373971859, 1],
    ['pistol.revolver', 649912614, 1],
    ['riot.helmet', 671063303, 8],
    ['roadsign.jacket', -2002277461, 2],
    ['roadsign.kilt', 1850456855, 2],
    ['roadsigns', 1199391518, 4],
    ['salvaged.sword', 1326180354, 12],
    ['scrap', -932201673, 12],
    ['shoes.boots', -1549739227, 8],
    ['supply.signal', 1397052267, 1],
    ['syringe.medical', 1079279582, 12],
    ['targeting.computer', 1523195708, 3],
    ['techparts', 73681876, 3],
    ['waterjug', -119235651, 8],
    ['weapon.mod.flashlight', 952603248, 6],
    ['weapon.mod.holosight', 442289265, 6],
    ['weapon.mod.lasersight', -132516482, 6],
    ['weapon.mod.silencer', -1850571427, 6],
    ['weapon.mod.simplesight', -855748505, 6]
]

number_of_sets = 200
max_item_types = 3

def weights_to_sized_weight(weights_data):
    tot_weight = 0
    cleaned_data_set = []
    for [name, item_id, weight] in weights_data:
        if weight != 0:
            cleaned_data_set.append([name, item_id, weight])

    for [_name, _item_id, weight] in cleaned_data_set:
        tot_weight += weight

    number_of_picks = max_item_types * number_of_sets
    relative_weight = number_of_picks / tot_weight

    # adjust them
    for [name, item_id, weight] in cleaned_data_set:
        yield [name, item_id, int(weight * relative_weight)] # Not sure what rounding to use so..

def generate_loot_tables(data_set):
    sets_dict = {}
    for x in range(number_of_sets):
        randomnumber = randint(1, 100)
        if randomnumber <= 75:
            items_in_set = 1
        elif randomnumber <= 90:
            items_in_set = 2
        else:
            items_in_set = 3
        if items_in_set != 0:
            picked_items = []
            for i in range(items_in_set):
                item_picked = data_set[randint(0, len(data_set)-1)]
                picked_items.append(item_picked[1])
                pos_in_list = data_set.index(item_picked)
                if item_picked[2] -1 == 0:
                    data_set.pop(pos_in_list)
                else:
                    data_set[pos_in_list] = [item_picked[0], item_picked[1], item_picked[2] -1]
            sets_dict[x] = picked_items
        else:
            sets_dict[x] = "0"
    return sets_dict

def write_to_json(loot_table):
    generated_scientist_dict = {}
    set_names = []
    for x in range(number_of_sets):
        set_names.append("scientist{}".format(x))
        scientist_base = {
            "name": "scientist{}".format(x),
            "description": None,
            "max": 0,
            "cooldown": 0.0,
            "authlevel": 0,
            "hide": False,
            "npconly": True,
            "permission": None,
            "image": None,
            "building": None,
        }
        scientist_item_base = [
            {
                "itemid": -253079493,
                "container": "wear",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -1812555177,
                "container": "belt",
                "amount": 1,
                "skinid": 1264358358,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -41440462,
                "container": "belt",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": 1326180354,
                "container": "belt",
                "amount": 1,
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
            {
                "itemid": -932201673,
                "container": "main",
                "amount": randint(2, 7),
                "skinid": 0,
                "weapon": False,
                "blueprintTarget": 0,
                "mods": []
            },
        ]
        loot_drop = []
        for item_id in loot_table[x]:
            if item_id != "0":
                amount = 1
                if item_id == -932201673 or item_id == 785728077 or item_id == -1211166256 or item_id == -1685290200:
                    amount = randint(1, 3)
                json_block = {
                    "itemid": item_id,
                    "container": "main",
                    "amount": amount,
                    "skinid": 0,
                    "weapon": False,
                    "blueprintTarget": 0,
                    "mods": []
                }
                loot_drop.append(json_block)
        scientist_items = scientist_item_base + loot_drop
        generated_scientist_dict["scientist{}".format(x)] = scientist_base
        generated_scientist_dict["scientist{}".format(x)]["items"] = scientist_items
    sillykitstring_dict = { "Kit": set_names}
    with open("scientist_set_names.json", "w") as namefile:
        json.dump(sillykitstring_dict, namefile)
    with open("scientist_generation.json", "w") as kitfile:
        json.dump(generated_scientist_dict, kitfile)

data_set = list(weights_to_sized_weight(fake_data))
write_to_json(generate_loot_tables(data_set))
